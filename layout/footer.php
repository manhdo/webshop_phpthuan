<!--start-footer-->
<div class="footer">
		<div class="container">
			<div class="footer-top">
				<div class="col-md-3 footer-left">
					<h3>Hệ thống</h3>
					<ul>
						<li><a href="#">Giới thiệu website</a></li>
						<li><a href="contact.php">Liên hệ chúng tôi</a></li>
						<li><a href="#">Trang Web chúng tôi</a></li>
						<li><a href="#">Trên bảng tin tức</a></li>
						<li><a href="#">Đội nhóm</a></li>
						<li><a href="#">Tuyển dụng</a></li>					 
					</ul>
				</div>
				<div class="col-md-3 footer-left">
					<h3>Tài khoản của bạn</h3>
					<ul>
						<li><a href="account.php"></a></li>
						<li><a href="#">Thông tin cơ bản</a></li>
						<li><a href="contact.php">Chi tiết</a></li>
						<li><a href="#">Giảm giá</a></li>
						<li><a href="#">Kiểm tra đặt hàng</a></li>					 					 
					</ul>
				</div>
				<div class="col-md-3 footer-left">
					<h3>Dịch vụ khách hàng</h3>
					<ul>
						<li><a href="#">Câu hỏi</a></li>
						<li><a href="#">Shipping</a></li>
						<li><a href="#">Cancellation</a></li>
						<li><a href="#">Trả lại hàng</a></li>
						<li><a href="#">Đặt đơn hàng</a></li>
						<li><a href="#">Khách hàng</a></li>					 
					</ul>
				</div>
				<div class="col-md-3 footer-left">
					<h3>Danh mục sản phẩm</h3>
					<ul>
						<li><a href="#">Áo</a></li>
						<li><a href="#">Kính mắt</a></li>
						<li><a href="#">Giày</a></li>
						<li><a href="#">Giày thể thao</a></li>			 
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-footer-->
	<!--end-footer-text-->
<script src="../script/js_User/ajax_products.js"></script>
	<div class="footer-text">
		<div class="container">
			<div class="footer-main">
				<p class="footer-class">© 2017 ataka All Rights | Design by  <a href="http://w3layouts.com/" target="_blank">ManhBker</a> </p>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
				};
				*/
										
				$().UItoTop({ easingType: 'easeOutQuart' });
										
			});
		</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
	<!--end-footer-text-->	
</body>
</html>