
function timkiem_sanpham(gia, loaisp){
	$.ajax({
		url :'ajax/TimKiemAjax.php',
		type : 'POST',
		data : {
			gia :  gia,
			loaisp : loaisp
		},
		success : function(result){
			$('#loadSP').html(result);
		}
	});
}

 /***Hàm đổi mật khẩu ajax**/ 
 function DoiMatKhau(tendangnhap, matkhaucu, matkhaumoi){
	$.ajax({
		url :'ajax/DoiMatKhauAjax.php',
		type : 'POST',
		data : {
			tendangnhap :  tendangnhap,
			matkhaucu : matkhaucu,
			matkhaumoi : matkhaumoi
		},
		success : function(result){
			$('#dmk_thongbao').html(result);
			matkhaucu = $('#matkhaucu').val('');
			matkhaumoi = $('#matkhaumoi').val('');
			nhaplaimatkhau = $('#nl_matkhaumoi').val('');
		}
	});
}

/***Hàm xử lý ajax giỏ hàng***/
function ThemGioHang(masanpham , soluong){
	// alert(masanpham);
	// alert(soluong);
	$.ajax({
		url :'ajax/ThemGioHangAjax.php',
		type : 'POST',
		data : {
			masanpham : masanpham,
			soluong :  soluong
		},
		success : function(result) {
			$('#divgiohang').html(result);
		}
	});
}


/***Hàm sửa giỏ hàng ajax giỏ hàng***/
function SuaGioHang(masanpham , soluong){
	// alert(masanpham);
	// alert(soluong);
	$.ajax({
		url :'ajax/SuaGioHangAjax.php',
		type : 'POST',
		data : {
			masanpham : masanpham,
			soluong :  soluong
		},
		success : function(result) {
			$('.in-check').html(result);
		}
	});
}

/***Hàm sửa giỏ hàng ajax giỏ hàng***/
function XoaGioHang(masanpham){
	// alert(masanpham);
	// alert(soluong);
	$.ajax({
		url :'ajax/XoaGioHangAjax.php',
		type : 'POST',
		data : {
			masanpham : masanpham
		},
		success : function(result) {
			$('.in-check').html(result);
		}
	});
}