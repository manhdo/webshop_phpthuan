<?php
	include("../layout/header_admin.php");
	if(!isset($_SESSION["admin"])){
		echo "<script>location='web/index.php'</script>";
	}
	$layLoaiSP = "SELECT * FROM loaisp";
	$truyvan_layloaiSP = mysql_query($layLoaiSP);
	
?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<!-- Page Heading !-->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">
						Thêm sản phẩm
					</h1>
					<ol class="breadcrumb">
			                           <li>
			                                <i class="fa fa-home"></i>  <a href="index.php">Trang chủ</a>
			                           </li>
			                           <li class="active">
			                                <i class="fa fa-file"></i><a href="SanPham.php"> Sản phẩm</a>
			                           </li>
			                           <li class="active">
			                                <i class="fa fa-file"></i> Thêm Sản phẩm
			                           </li>
		                        	</ol>
				</div>
			</div>
			<div class="col-lg-12">
				<div>
				<!-- REQUEST_URI cũng giống như PHP_SELF.Nhưng khác PHP_SELF là REQUEST_URI có thể lấy tất cả url của website vd:http://abc.php?x=123 còn PHP_SELF chỉ có thể lấy đến .php(http//abc.php) -->
					<form id="submitForm" method= "POST" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
						<table class="table table-bordered">
			                                   <tr>
			                                        <th>Tên sản phẩm</th>
			                                        <td>
			                                        	<input id="tensanpham" name="tensanpham" class="form-control"  />
			                                        </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Số lượng</th>
			                                        <td>
			                                             <input id="soluong" name="soluong" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Ảnh 1</th>
			                                        <td>
			                                             <input id="anh1" name="anh1" type="file" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Ảnh 2</th>
			                                        <td>
			                                             <input id="anh2" name="anh2" type="file" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Ảnh 3</th>
			                                        <td>
			                                             <input id="anh3" name="anh3" type="file" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Đơn giá</th>
			                                        <td>
			                                             <input id="dongia" name="dongia" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Thông tin</th>
			                                        <td>
			                                             <input id="thongtin" name="thongtin" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Trạng thái</th>
			                                        <td>
			                                             <input id="trangthai" name="trangthai" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Danh mục</th>
			                                        <td>
			                                            <select name="loaisp" id="loaisp" class="form-control">
			                                            	<?php 
			                                            	   while($cotloaisp = mysql_fetch_array($truyvan_layloaiSP)){
			                                            	?>  	
			                                            	   	<option value="<?php echo $cotloaisp["MaLoaiSP"]; ?>"><?php echo $cotloaisp["TenLoai"]; ?></option>
			                                            	<?php  } ?>
			                                            </select>
			                                         </td>
			                                   </tr>
			                                    <tr>
			                                        <th></th>
			                                        <th>
			                                        	<input id="kqua"  class="btn btn-primary" type="submit" value="Lưu" />
			                                        </th>
			                                   </tr>
			                            </table>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#kqua').click(function(){
				// alert("đã click xem");
				tensanpham = $('#tensanpham').val();
				soluong = $('#soluong').val();
				anh1 = $('#anh1').val();
				anh2 = $('#anh2').val();
				anh3 = $('#anh3').val();
				dongia = $('#dongia').val();
				thongtin = $('#thongtin').val();
				trangthai = $('#trangthai').val();
				loaisp = $('#loaisp').val();
				loi = 0;
				//alert(anh1);
				//alert(tensanpham);
				if(tensanpham == "" || soluong == "" || anh1 == "" || anh2 == "" || anh3 =="" || dongia == "" || thongtin == "" || trangthai=="" || loaisp =="" ){
					loi ++;
					alert('Hãy nhập đầy đủ thông tin');
				}
				/*để else return false là sẽ bị chết hoàn toàn phía dưới..chú ý khi dùng return false*/
				// }else{
				// 	return false;
				// }			
			});
		});
	</script>
<?php 
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$tensanpham = $_POST["tensanpham"];
		$soluong = $_POST["soluong"];
		$anh1 = $_FILES["anh1"];
		$anh2 = $_FILES["anh2"];
		$anh3 = $_FILES["anh3"];
		$dongia = $_POST["dongia"];
		$thongtin = $_POST["thongtin"];
		$trangthai = $_POST["trangthai"];
		$loaisp = $_POST["loaisp"];

		if($anh1["name"] == "" &&  $anh2["name"] == "" && $anh3["name"] == ""){
			echo "<script>alert('Bạn cần chọn đầy đủ hình');</script>";
			return;//kết thúc ngay vòng check if.(sẽ ko cần else).
		}

		if($anh1["type"] != "image/jpg" && $anh1["type"] != "image/png"){
			echo "<script>alert('Bạn cần chọn đúng dạng hình ảnh 1');</script>";
			// return;//kết thúc ngay vòng check if.(sẽ ko cần else).
		}
		if($anh1["type"] != "image/jpg" && $anh2["type"] != "image/png"){
			echo "<script>alert('Bạn cần chọn đúng dạng hình ảnh 2');</script>";
			// return;//kết thúc ngay vòng check if.(sẽ ko cần else).
		}

		if($anh3["type"] != "image/jpg" && $anh3["type"] != "image/png"){
			echo "<script>alert('Bạn cần chọn đúng dạng hình ảnh 3');</script>";
			//return;
		}
		// echo 	$tensanpham." tensanpham";
		// echo 	$dongia." dongia";
		// echo 	$anh2["name"]." anhso2";
		// echo 	$thongtin." thongtin";
		// echo 	$trangthai." trangthai";
		// echo 	$loaisp." loaisp";
/*Hàm đưa vào folder chứa file ảnh*/
		move_uploaded_file($anh1["tmp_name"], "../image/images_User/HinhSP/".$anh1["name"]);
		move_uploaded_file($anh2["tmp_name"], "../image/images_User/HinhSP/".$anh2["name"]);
		move_uploaded_file($anh3["tmp_name"], "../image/images_User/HinhSP/".$anh3["name"]);
/*Đưa vào csdl*/
	$themdulieu = "INSERT INTO sanpham(TenSanPham , SoLuong , Anh , Anh2 , Anh3 , DonGia , ThongTin , TrangThai , MaLoaiSP)
VALUES('".$tensanpham."' , '".$soluong."' , '".$anh1["name"]."' , '".$anh2["name"]."' , '".$anh3["name"]."' , '".$dongia."' , '".$thongtin."' , '".$trangthai."' , '".$loaisp."')";
	// print_r($themdulieu);exit();
	if(mysql_query($themdulieu)) {
		echo "<script>alert('Bạn đã thêm thành công sản phẩm');</script>";
		echo "<script>location='SanPham.php';</script>";
	}else {
		echo "<script>alert('Bạn đã thêm lỗi');</script>";
	}
	/*doan script do o dau vay?*/

}
?>
<?php
	include("../layout/footer_admin.php");

//$_FILES["Anh"]["type"]!= "image/jpeg";
//$_FILES["spAnh"]["type"] != "image/png";
//file_exists()
//unlink()
?>

