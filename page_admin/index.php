<?php 
	include("../layout/header_admin.php");
	if(!isset($_SESSION["admin"]))
		echo "<script>location='web/index.php';</script>";
?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<!-- Page Heading !-->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">
						Trang chủ
					</h1>
					<ol class="breadcrumb">
						<li>
							<i class="fa fa-home"></i><a href="DanhMucSanPham.php">Trang Chủ</a>
							<div class="jumbotron">
						                  <h1>Hello, world!</h1>
						                  <p>
						                  	Đây là hệ thống admin của ataka.com
						                  	Chào mừng admin đến với hệ thống.<br />
						                  	Hệ thống ataka.com là hệ thống website bán hàng đang hot nhất hiện nay được tập đoàn vingroup đầu tư với giá 1 triệu $.Hệ thống đang được đưa vào sử dụng với số lượt truy cập vô cùng cao trong ngày.
						                  </p>
                						</div>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
<?php
	include("../layout/footer_admin.php");
?>