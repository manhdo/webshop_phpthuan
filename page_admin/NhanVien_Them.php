<?php
	include("../layout/header_admin.php");
	if(!isset($_SESSION["admin"])){
		echo "<script>location='web/index.php'</script>";
	}
	

?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<!-- Page Heading !-->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">
						Thêm nhân viên
					</h1>
					<ol class="breadcrumb">
			                           <li>
			                                <i class="fa fa-home"></i>  <a href="index.php">Trang chủ</a>
			                           </li>
			                           <li class="active">
			                                <i class="fa fa-file"></i><a href="NhanVien.php"> Nhân Viên</a>
			                           </li>
			                           <li class="active">
			                                <i class="fa fa-file"></i>  Thêm nhân viên
			                           </li>
		                        	</ol>
				</div>
			</div>
			<div class="col-lg-12">
				<div>
				<!-- REQUEST_URI cũng giống như PHP_SELF.Nhưng khác PHP_SELF là REQUEST_URI có thể lấy tất cả url của website vd:http://abc.php?x=123 còn PHP_SELF chỉ có thể lấy đến .php(http//abc.php) -->
					<form method= "POST" action="<?php echo $_SERVER["REQUEST_URI"]; ?>">
						<table class="table table-bordered">
			                                   <tr>
			                                        <th>Họ Tên</th>
			                                        <td>
			                                        	<input id="hoten" name="hoten" class="form-control"  />
			                                        </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Tên đăng nhập</th>
			                                        <td>
			                                             <input id="tendangnhap" name="tendangnhap" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Mật khẩu</th>
			                                        <td>
			                                             <input id="matkhau" name="matkhau" type="password"  class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Ngày sinh</th>
			                                        <td>
			                                             <input id="ngaysinh" name="ngaysinh" type="date" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Giới tính</th>
			                                        <td>
			                                             <input id="gioitinh" name="gioitinh" type="radio" value="M" checked> Nam
								  <input  id="gioitinh" name="gioitinh" type="radio" value="F"> Nữ
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Điện thoại</th>
			                                        <td>
			                                             <input id="dienthoai" name="dienthoai" class="form-control"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th></th>
			                                        <th>
			                                        	<input id="Luu"  class="btn btn-primary" type="submit" value="Lưu" />
			                                        </th>
			                                   </tr>
			                            </table>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#Luu').click(function() {
				hoten = $('#hoten').val();
				tendangnhap = $('#tendangnhap').val();
				matkhau = $('#matkhau').val();
				ngaysinh = $('#ngaysinh').val();
				gioitinh  = $('#gioitinh').val();
				dienthoai = $('#dienthoai').val();
				loi = 0;
				if(hoten=="" || tendanhmuc == "" || matkhau == "" || gioitinh == "" || ngaysinh=="" || dienthoai == ""){
					loi ++;
					alert("Hãy nhập đầy đủ thông tin");
				}
				if(loi!=0) {
					return false;
				}
			});
		});
	</script>
	
<?php 
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$hoten = $_POST["hoten"];
		$tendangnhap = $_POST["tendangnhap"];
		$matkhau = $_POST["matkhau"];
		$ngaysinh = $_POST["ngaysinh"];
		$gioitinh = $_POST["gioitinh"];
		$dienthoai = $_POST["dienthoai"];

		$themdl_nv = "INSERT INTO nhanvien(HoTen , TenDangNhap, MatKhau, NgaySinh, GioiTinh, DienThoai) 
		VALUES('".$hoten."' , '".$tendangnhap."' , '".$matkhau."' , '".$ngaysinh."' , '".$gioitinh."' , '".$dienthoai."')";
		// print_r($themdl_nv); exit();
		if(mysql_query($themdl_nv)) {
			echo "<script>alert('Thêm dữ liệu thành công');</script>";
			echo "<script>location='NhanVien.php';</script>";
		}else {
			echo "<script>alert('Đã xảy ra lỗi');</script>";
		}
	}
?>
<?php
	include("../layout/footer_admin.php");

?>