<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
	include_once('../layout/header.php');
	/*Hủy session*/
	//unset($_SESSION["tendangnhap"]);
/*check session nếu người dùng đã đăg nhập thì chuyển qua luôn trang sản phẩm*/
	if(!isset($_SESSION["tendangnhap"])) 
		echo "<script>location='products.php';</script>";

		//$layThongTin = "SELECT * FROM thanhvien WHERE TenDangNhap = '".$_SESSION["tendangnhap"]."'";
		$layThongTin = "SELECT * FROM `thanhvien` WHERE TenDangNhap='".$_SESSION["tendangnhap"]."' ";
		$truyvanLayThongTin = mysql_query($layThongTin);
		$cot =  mysql_fetch_array($truyvanLayThongTin);
		// echo $cot;


?>
	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="index.php">Trang chủ</a></li>
					<li class="active">Thông tin tài khoản</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<!--start-account-->
	<div class="account">
		<div class="container"> 
			<div class="account-bottom">
				<div class="col-md-6 account-left">
					<form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<div class="account-top heading">
							<h3 style="margin-bottom: 20px;">Thông Tin Tài Khoản</h3>
							<a href="#" id="link_doimatkhau">Đổi mật khẩu</a><br />
							<a href="#" id="link_doithongtin_tk">Thay đổi thông tin tài khoản</a> 
						</div>
						<div class="address">
							<span>Tên đăng nhập</span>
							<input id="tendangnhap" type="hidden" value="<?php echo $cot["TenDangNhap"]; ?>" />
							<div class="info_username">
								<p><?php echo $cot["TenDangNhap"]; ?></p>
							</div>
						</div>
						<div class="address">
							<span>Mật Khẩu</span>
							<!-- <input id="matkhau" name="matkhau" type="password" /> -->
							<div class="info_username">
								<p>********</p>
							</div>
						</div>
						<div class="address">
							<span>Họ Tên</span>
							<!-- <input id="hoten" name="hoten" type="text"> -->
							<div class="info_username">
								<p><?php echo $cot["HoTen"]; ?></p>
							</div>
						</div>
						<div class="address">
							<span>Ngày sinh</span>
							<!-- <p><?php echo $cot["NgaySinh"]; ?></p> -->
							<div class="info_username">
								<p>
									<?php
										echo date("d/m/Y", strtotime($cot["NgaySinh"])) ;
									?>
								</p>
							</div>
						</div>
						<div class="address">
							<span>Giới tính</span>
							<!-- <input id="gioitinh" name="gioitinh" type="radio" value="M" checked> Nam -->
							<div class="info_username">
								<p>
									<?php
										if($cot["GioiTinh"] == "F"){
											echo "Nữ";
										}else{
											echo "Nam";
										}
									?>
								</p>
							</div>
						</div>
						<div class="address">
							<span>Địa chỉ</span>
							<!-- <input id="diachi" name="diachi" type="text"> -->
							<div class="info_username">
								<p><?php echo $cot["DiaChi"]; ?></p>
							</div>
						</div>
						<div class="address">
							<span>Điện Thoại</span>
							<!-- <input id="dienthoai" name="dienthoai" type="text"> -->
							<div class="info_username">
								<p><?php echo $cot["DienThoai"]; ?></p>
							</div>
						</div>
						<div class="address">
							<span>Email</span>
							<!-- <input id="email" type="email" name="email" required placeholder="Enter you Email" /> -->
							<div class="info_username">
								<p><?php echo $cot["Email"]; ?></p>
							</div>
						</div>
					</form>
				</div>
				<style type="text/css">
					.info_username{
						border: 1px solid #968a8a;
    						padding: 9px;
					}
					.info_username p {
						margin: 0px;
						font-size: 14px;
					}
				</style>
				<!--Div chứa thay đổi mật khẩu người dùng!-->
				<div class="col-md-6 account-left" id="thaydoimatkhau" style="display:none;">
						<input type="hidden" name="tranghientai" value="<?php echo $_SERVER["PHP_SELF"]; ?>" />
						<div class="account-top heading">
							<h3>Đổi mật khẩu</h3>
						</div>
						<div class="address">
							<span>Mật khẩu cũ</span>
							<input id="matkhaucu" required type="password" />
						</div>
						<div class="address">
							<span>Mật Khẩu mới </span>
							<input id="matkhaumoi" required  type="password" />
						</div>
						<div class="address">
							<span>Nhập lại Mật Khẩu mới </span>
							<input id="nl_matkhaumoi" required  type="password" />
						</div>
						<div class="address">
							<span id="dmk_thongbao" style="color:red;font-size:18px;"></span>
							<input id="doimatkhau" type="submit"  value="Đổi mật khẩu">
						</div>
				</div>
				<!--Div chứa form thay đổi thông tin người dùng!-->
				<div class="col-md-6 account-left" id="thaydoithongtin_tk" style="display:none;">
					<form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
						<input name="tendangnhap" type="hidden" value="<?php echo $cot["TenDangNhap"]; ?>" />
						<div class="account-top heading">
							<h3>Thay Đổi Thông Tin Tài Khoản</h3>
						</div>
						<div class="address">
							<span>Họ Tên</span>
							<input id="hoten" name="hoten" type="text" required value="<?php echo $cot["HoTen"] ?>">
						</div>
						<div class="address">
							<span>Ngày sinh</span>
							<input id="ngaysinh" name="ngaysinh"  required type="date" value="<?php echo $cot["NgaySinh"]?>" />
						</div>

						<!-- Đoạn code check select radio khá là hay -->
						<div class="address">
							<span>Giới tính</span>
							<p>
								<?php
									if($cot["GioiTinh"] == "F"){ ?>
										<input name="gioitinh" type="radio" value="M"> Nam
										<input  name="gioitinh" type="radio" value="F" checked> Nữ
								<?php }else{
								?>
										<input name="gioitinh" type="radio" value="M" checked> Nam
										<input  name="gioitinh" type="radio" value="F"> Nữ
								<?php 
									}
								?>
							</p>
						</div>
						<div class="address">
							<span>Địa chỉ</span>
							<input id="diachi" name="diachi" type="text" required value="<?php echo $cot["DiaChi"];?>" />
						</div>
						<div class="address">
							<span>Điện Thoại</span>
							<input id="dienthoai" name="dienthoai" required type="text" value="<?php echo $cot["DienThoai"];?>" />
						</div>
						<div class="address">
							<span>Email</span>
							<input id="email" type="email" name="email" required placeholder="Enter you Email" value="<?php echo $cot["Email"]; ?>" />
						</div>
						<div class="address">
							<span id="tdtt_thongbao" style="color:red;font-size:18px;"></span>
						</div>
						<div class="address new">
							<input id="doithongtin" type="submit" value="Thay Đổi" style="float:right;" />
						</div>
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>

		<!--Check cho form thay đổi thông tin!-->
<script type="text/javascript">
	$(document).ready(function(){
		$('#doithongtin').click(function(){
			hoten = $('#hoten').val();
			ngaysinh = $('#ngaysinh').val();
			diachi = $('#diachi').val();
			dienthoai = $('#dienthoai').val();
			email = $('#email').val();
			//alert(dienthoai);

			loi = 0;
			if(hoten=='' || ngaysinh=='' || diachi =='' || dienthoai=='' || email==''){
				loi ++;
				$('#tdtt_thongbao').text("Hãy nhập đầy đủ thông tin");
			}

			if(isNaN(dienthoai)){
				loi ++;
				$('#tdtt_thongbao').text("Điện thoại phải nhập dưới dạng số");
			}

			/*return false là return trả về sai lỗi..nó sẽ reload lại trang đăng ký cho người dùng*/
			if(loi !=0 ){
				return false;
			}
		});
	});
</script>

<!-- Hiện ẩn 2 div chỉnh sửa mật khẩu và chỉnh sửa thông tin cá nhân -->
<script src="../script/js_User/ajax_products.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#link_doimatkhau').click(function(){
			$('#thaydoimatkhau').show();
			$('#thaydoithongtin_tk').hide();
		});

		$('#link_doithongtin_tk').click(function(){
			$('#thaydoithongtin_tk').show();
			$('#thaydoimatkhau').hide();
		});

		$(document).ready(function(){
			$('#doimatkhau').click(function(){
				matkhaucu = $('#matkhaucu').val();
				matkhaumoi = $('#matkhaumoi').val();
				nhaplaimatkhau = $('#nl_matkhaumoi').val();
				loi = 0;
				if(matkhaucu == '' || matkhaumoi=='' || nhaplaimatkhau=='' ){
					loi ++;
					$('#dmk_thongbao').text("Hãy nhập đầy đủ thông tin");
				}

				if(matkhaumoi != nhaplaimatkhau){
					loi ++;
					$('#dmk_thongbao').text("Mật khẩu nhập lại chưa trùng khớp");
				}
			/*return false là return trả về sai lỗi..nó sẽ reload lại trang đăng ký cho người dùng*/
				if(loi !=0 ){
					return false;
				}else {
					tendangnhap = $('#tendangnhap').val();
					$('#dmk_thongbao').text("");
					DoiMatKhau(tendangnhap , matkhaucu ,matkhaumoi);
				}	

			});
		});
	});
</script>

<!--Code check server để update thông tin chỉnh sửa!-->
<?php
	
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$tendangnhap = $_POST["tendangnhap"];
		$hoten = $_POST["hoten"];
		$ngaysinh = $_POST["ngaysinh"];
		$gioitinh =  $_POST["gioitinh"];
		$diachi = $_POST["diachi"];
		$dienthoai = $_POST["dienthoai"];
		$email = $_POST["email"];

		//check email theo kiểu php
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			echo "<script>alert('Email không hợp lệ');</script>";
		} else{
		
		//echo "Da ket noi csld";
		$capnhatThongTin = "UPDATE thanhvien SET HoTen='".$hoten."' , NgaySinh='".$ngaysinh."' , GioiTinh='".$gioitinh."' ,
		DiaChi = '".$diachi."', DienThoai='".$dienthoai."' , Email = '".$email."'  WHERE TenDangNhap='".$tendangnhap."' ";

			if(mysql_query($capnhatThongTin)) {
				 echo "<script>alert('Thay đổi thành công');location='ThongTinTaiKhoan.php';</script>";
			}else {
				 echo "<script>alert('Xảy ra lỗi');</script>";
			}

		}
	}
?>
	<!--end-account-->
	<!--start-footer-->
<?php
	include_once('../layout/footer.php');
?>