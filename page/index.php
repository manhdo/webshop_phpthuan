<?php
	include_once('../layout/header.php');

	/*Lấy 2 sản phẩm có giá cao nhất cho vào 2 bên left right*/
	$laysp1 = "SELECT * FROM sanpham ORDER BY DonGia DESC LIMIT 0,1";
	$truyvan_LaySPCao1 = mysql_query($laysp1);
	$cot1 = mysql_fetch_array($truyvan_LaySPCao1); 
	/*------------ASC: chiều tăng dần, DESC: chiều giảm dần----------------*/
/*Lấy sản phẩm cao thứ 2*/
	$laysp2 = "SELECT * FROM sanpham ORDER BY DonGia DESC LIMIT 1,1";
	$truyvan_LaySPCao2 = mysql_query($laysp2);
	$cot2 = mysql_fetch_array($truyvan_LaySPCao2); 

/*Lấy sản phẩm show ra màn hình chính trang chủ*/
	$laySP = "SELECT * FROM sanpham ORDER BY SoLuong DESC LIMIT 0,8";
	$truyvan_LaySP = mysql_query($laySP);
	
?>
	<!--banner-starts-->
	<div class="bnr" id="home">
		<div  id="top" class="callbacks_container">
			<ul class="rslides" id="slider4">
			    <li>
					<div class="banner-1"></div>
				</li>
				<li>
					<div class="banner-2"></div>
				</li>
				<li>
					<div class="banner-3"></div>
				</li>
			</ul>
		</div>
		<div class="clearfix"> </div>
	</div>
	<!--banner-ends--> 
	<!--Slider-Starts-Here-->
		<script src="../script/js_User/responsiveslides.min.js"></script>
			 <script>
			    // You can also use "$(window).load(function() {"
			    $(function () {
			      // Slideshow 4
			      $("#slider4").responsiveSlides({
			        auto: true,
			        pager: true,
			        nav: false,
			        speed: 500,
			        namespace: "callbacks",
			        before: function () {
			          $('.events').append("<li>before event fired.</li>");
			        },
			        after: function () {
			          $('.events').append("<li>after event fired.</li>");
			        }
			      });
			
			    });
			  </script>
			<!--End-slider-script-->
	<!--start-banner-bottom--> 
	<div class="banner-bottom">
		<div class="container">
			<div class="banner-bottom-top">
				<div class="col-md-6 banner-bottom-left">
					<div class="bnr-one">
						<div class="bnr-left">
							<h1><a href="ChiTietSanPham.php?MaSP=<?php echo $cot1["MaSanPham"];?>"><?php echo $cot1["TenSanPham"]; ?></a></h1>
							<p><?php echo $cot1["ThongTin"]; ?></p>
							<div class="b-btn"> 
								<a href="ChiTietSanPham.php?MaSP=<?php echo $cot1["MaSanPham"];?>">Mua Ngay</a>
							</div>
						</div>
						<div class="bnr-right"> 
							<a href="ChiTietSanPham.php?MaSP=<?php echo $cot1["MaSanPham"];?>"><img style="width:150px;height:150px;" src="../image/images_User/HinhSP/<?php echo $cot1["Anh"];?>" alt="" /></a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-6 banner-bottom-left">
					<div class="bnr-one">
						<div class="bnr-left">
							<h1><a href="ChiTietSanPham.php?MaSP=<?php echo $cot2["MaSanPham"];?>"><?php echo $cot2["TenSanPham"]; ?></a></h1>
							<p><?php echo $cot2["ThongTin"]; ?></p>
							<div class="b-btn"> 
								<a href="ChiTietSanPham.php?MaSP=<?php echo $cot2["MaSanPham"];?>">Mua Ngay</a>
							</div>
						</div>
						<div class="bnr-right"> 
							<a href="ChiTietSanPham.php?MaSP=<?php echo $cot1["MaSanPham"];?>"><img style="width:150px;height:150px;" src="../image/images_User/HinhSP/<?php echo $cot2["Anh"];?>" alt="" /></a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-banner-bottom--> 
	<!--start-shoes--> 
	<div class="shoes"> 
		<div class="container"> 
			<div class="product-one">
			<?php 
				while ($cot = mysql_fetch_array($truyvan_LaySP)) { ?>
				<div class="col-md-3 product-left"> 
					<div class="p-one simpleCart_shelfItem">							
							<a href="ChiTietSanPham.php?MaSP=<?php echo $cot["MaSanPham"];?>">
								<img style="width:215px;height:180px;" src="../image/images_User/HinhSP/<?php echo $cot["Anh"]; ?>" alt="" />
								<div class="mask">
									<span>Chi tiết</span>
								</div>
							</a>
						<h4><?php $cot["TenSanPham"] ?></h4>
						<p><a class="item_add" href="ChiTietSanPham.php?MaSP=<?php echo $cot["MaSanPham"];?>"><i></i> <span class=" item_price"><?php echo DinhDangTien($cot["DonGia"]); ?> VNĐ</span></a></p>
					</div>
				</div>
			<?php }?>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-shoes-->
	<!--start-abt-shoe-->
	<div class="abt-shoe">
		<div class="container"> 
			<div class="abt-shoe-main">
				<div class="col-md-4 abt-shoe-left">
					<div class="abt-one">
						<a href="single.php"><img src="../image/images_User/abt-1.jpg" alt="" /></a>
						<h4><a href="single.php">SHOE ATAKA</a></h4>
						<p>Trẻ trung, năng động, nam tính và sành điệu cùng Giày nam ataka. Kiểu dáng sang trọng, đẳng cấp cùng chất liệu cao cấp, mềm mại, không mang đến cảm giác đau chân khi phải di chuyển nhiều, sản phẩm tạo nên sự tin tưởng tuyệt đối trong sự lựa chọn của giới trẻ thời nay. </p>
					</div>
				</div>
				<div class="col-md-4 abt-shoe-left">
					<div class="abt-one">
						<a href="single.php"><img src="../image/images_User/abt-2.jpg" alt="" /></a>
						<h4><a href="single.php">SHOE ATAKA</a></h4>
						<p>Trẻ trung, năng động, nam tính và sành điệu cùng Giày nam ataka. Kiểu dáng sang trọng, đẳng cấp cùng chất liệu cao cấp, mềm mại, không mang đến cảm giác đau chân khi phải di chuyển nhiều, sản phẩm tạo nên sự tin tưởng tuyệt đối trong sự lựa chọn của giới trẻ thời nay. </p>
					</div>
				</div>
				<div class="col-md-4 abt-shoe-left">
					<div class="abt-one">
						<a href="single.php"><img src="../image/images_User/abt-3.jpg" alt="" /></a>
						<h4><a href="single.php">SHOE ATAKA</a></h4>
						<p>Trẻ trung, năng động, nam tính và sành điệu cùng Giày nam ataka. Kiểu dáng sang trọng, đẳng cấp cùng chất liệu cao cấp, mềm mại, không mang đến cảm giác đau chân khi phải di chuyển nhiều, sản phẩm tạo nên sự tin tưởng tuyệt đối trong sự lựa chọn của giới trẻ thời nay. </p>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!--end-abt-shoe-->
	<!--start-footer-->
<?php
	include_once('../layout/footer.php');
?>
	