<?php
	include("../layout/header_admin.php");
	if(!isset($_SESSION["admin"])){
		echo "<script>location='web/index.php'</script>";
	}
/*Query này dùng để lấy ra thông tin của 1 sản phẩm cho vào value của input*/
	if(!isset($_GET["MaSanPham"])) {
		echo "<script>location='SanPham.php';</script>";
	}
	$layDuLieuSP = "SELECT * FROM sanpham WHERE MaSanPham='".$_GET["MaSanPham"]."'  ";
	$truyvan_laySP = mysql_query($layDuLieuSP);
	if(mysql_num_rows($truyvan_laySP) > 0) {
		$cot_sp = mysql_fetch_array($truyvan_laySP);
	}else{
		echo "<script>location='SanPham.php';</script>";
	}

/*Query này dùng để lấy ra select mã loại sản phẩm*/
	$layLoaiSP = "SELECT * FROM loaisp";
	$truyvan_layloaiSP = mysql_query($layLoaiSP);
	
?>
	<div id="page-wrapper">
		<div class="container-fluid">
			<!-- Page Heading !-->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">
						Sửa Sản phẩm
					</h1>
					<ol class="breadcrumb">
			                           <li>
			                                <i class="fa fa-home"></i>  <a href="index.php">Trang chủ</a>
			                           </li>
			                           <li class="active">
			                                <i class="fa fa-file"></i><a href="SanPham.php"> Sản phẩm</a>
			                           </li>
			                           <li class="active">
			                                <i class="fa fa-file"></i> Sửa Sản phẩm
			                           </li>
		                        	</ol>
				</div>
			</div>
			<div class="col-lg-12">
				<div>
				<!-- REQUEST_URI cũng giống như PHP_SELF.Nhưng khác PHP_SELF là REQUEST_URI có thể lấy tất cả url của website vd:http://abc.php?x=123 còn PHP_SELF chỉ có thể lấy đến .php(http//abc.php) -->
					<form id="submitForm" method= "POST" action="<?php echo $_SERVER["REQUEST_URI"]; ?>" enctype="multipart/form-data">
						<table class="table table-bordered">
			                                   <tr>
			                                        <th>Tên sản phẩm</th>
			                                        <td>
			<!-- Câu lệnh này rất quan trọng vì nó giữ lại trạng thái nhập dữ liệu cho người dùng khi submit.Nếu người dùng lỡ submit thì nó vẫn giữ lại giá trị khi load trang mà ko phải dữ liệu từ csdl đổ ra -->
			<!--Kiểm tra điều kiện..Nếu không có $_POST["tensanpham"] thì load lấy từ csdl ra bằng  $cot_sp["TenSanPham"].Còn không thì giữ nguyên $_POST["tensanpham"] khi nhập vào  !-->
			                                        	<input id="tensanpham" name="tensanpham" class="form-control" value="<?php echo empty($_POST["tensanpham"]) ? $cot_sp["TenSanPham"]:$_POST["tensanpham"]; ?>"  />
			                                        </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Số lượng</th>
			                                        <td>
			                                             <input id="soluong" name="soluong" class="form-control"  value="<?php echo empty($_POST["soluong"]) ? $cot_sp["SoLuong"]:$_POST["soluong"]; ?>"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                   	       <th></th>
			                                   	       <td>
			                                   	       		<img src="../image/images_User/HinhSP/<?php echo $cot_sp["Anh"]; ?>" style="width:50px; height:50px;" >
			                                   	       		<img src="../image/images_User/HinhSP/<?php echo $cot_sp["Anh2"]; ?>" style="width:50px; height:50px; margin-left:10px;" >
			                                   	       		<img src="../image/images_User/HinhSP/<?php echo $cot_sp["Anh3"]; ?>" style="width:50px; height:50px; margin-left:10px;" >
			                                   	       </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Ảnh 1</th>
			                                        <td>
			                                             <input id="anh1" name="anh1" type="file" class="form-control"   />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Ảnh 2</th>
			                                        <td>
			                                             <input id="anh2" name="anh2" type="file" class="form-control" />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Ảnh 3</th>
			                                        <td>
			                                             <input id="anh3" name="anh3" type="file" class="form-control" />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Đơn giá</th>
			                                        <td>
			                                             <!-- <input id="dongia" name="dongia" class="form-control" value="<?php echo empty($_POST["dongia"]) ? DinhDangTien($cot_sp["DonGia"])." VNĐ"  :$_POST["dongia"]; ?>" />  -->
			                                         	<input id="dongia" name="dongia" class="form-control" value="<?php echo empty($_POST["dongia"]) ?  $cot_sp["DonGia"] : $_POST["dongia"]; ?>" /> 
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Thông tin</th>
			                                        <td>
			                                             <input id="thongtin" name="thongtin" class="form-control" value="<?php echo empty($_POST["thongtin"]) ? $cot_sp["ThongTin"]:$_POST["thongtin"]; ?>"   />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Trạng thái</th>
			                                        <td>
			                                             <input id="trangthai" name="trangthai" class="form-control"  value="<?php echo empty($_POST["trangthai"]) ? $cot_sp["TrangThai"]:$_POST["trangthai"]; ?>"  />
			                                         </td>
			                                   </tr>
			                                   <tr>
			                                        <th>Danh mục</th>
			                                        <td>
			                                            <select name="loaisp" id="loaisp" class="form-control">
			                                            	<?php 
			                                            	   while($cotloaisp = mysql_fetch_array($truyvan_layloaiSP)){
			                                            	   	if($cotloaisp["MaLoaiSP"] == $cot_sp["MaLoaiSP"])
			                                            	   	{
			                                            	?>  	
			                                            	   	<option selected value="<?php echo $cotloaisp["MaLoaiSP"]; ?>"><?php echo $cotloaisp["TenLoai"]; ?></option>
			                                            	   	<?php } else{ ?>
			                                            	   	<option value="<?php echo $cotloaisp["MaLoaiSP"]; ?>"><?php echo $cotloaisp["TenLoai"]; ?></option>
			                                            	<?php } } ?>
			                                            </select>
			                                         </td>
			                                   </tr>
			                                    <tr>
			                                        <th></th>
			                                        <th>
			                                        	<input id="kqua"  class="btn btn-primary" type="submit" value="Lưu" />
			                                        </th>
			                                   </tr>
			                            </table>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#kqua').click(function(){
				// alert("đã click xem");
				tensanpham = $('#tensanpham').val();
				soluong = $('#soluong').val();
				anh1 = $('#anh1').val();
				anh2 = $('#anh2').val();
				anh3 = $('#anh3').val();
				dongia = $('#dongia').val();
				thongtin = $('#thongtin').val();
				trangthai = $('#trangthai').val();
				loaisp = $('#loaisp').val();
				loi = 0;
				//alert(anh1);
				//alert(tensanpham);
				if(tensanpham == "" || soluong == "" || anh1 == "" || anh2 == "" || anh3 =="" || dongia == "" || thongtin == "" || trangthai=="" || loaisp =="" ){
					loi ++;
					alert('Hãy nhập đầy đủ thông tin');
				}
				/*để else return false là sẽ bị chết hoàn toàn phía dưới..chú ý khi dùng return false*/
				// }else{
				// 	return false;
				// }			
			});
		});
	</script>
<?php 
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$tensanpham = $_POST["tensanpham"];
		$soluong = $_POST["soluong"];
		$anh1 = $cot_sp["Anh"];
		$anh2 = $cot_sp["Anh2"];
		$anh3 = $cot_sp["Anh3"];
		$dongia = $_POST["dongia"];
		$thongtin = $_POST["thongtin"];
		$trangthai = $_POST["trangthai"];
		$loaisp = $_POST["loaisp"];

		// if($anh1["name"] == "" &&  $anh2["name"] == "" && $anh3["name"] == ""){
		// 	echo "<script>alert('Bạn cần chọn đầy đủ hình');</script>";
		// 	return;//kết thúc ngay vòng check if.(sẽ ko cần else).
		// }

		// if($anh1["type"] != "image/jpg" && $anh1["type"] != "image/png"){
		// 	echo "<script>alert('Bạn cần chọn đúng dạng hình ảnh 1');</script>";
		// 	// return;//kết thúc ngay vòng check if.(sẽ ko cần else).
		// }
		// if($anh2["type"] != "image/jpg" && $anh2["type"] != "image/png") {
		// 	echo "<script>alert('Bạn cần chọn đúng dạng hình ảnh 2');</script>";
		// }
		// if($anh3["type"] != "image/jpg" && $anh3["type"] != "image/png"){
		// 	echo "<script>alert('Bạn cần chọn đúng dạng hình ảnh 3');</script>";
		// 	return;
		// }
		if($_FILES["anh1"]["name"] != ""){
			unlink("../image/images_User/HinhSP/".$anh1);
			$anh1 = $_FILES["anh1"]["name"];
			move_uploaded_file($_FILES["anh1"]["tmp_name"], "../image/images_User/HinhSP/".$anh1);
		}
		if($_FILES["anh2"]["name"] != ""){
			unlink("../image/images_User/HinhSP/".$anh2);
			$anh2 = $_FILES["anh2"]["name"];
			move_uploaded_file($_FILES["anh2"]["tmp_name"], "../image/images_User/HinhSP/".$anh2);
		}
		if($_FILES["anh3"]["name"] != ""){
			unlink("../image/images_User/HinhSP/".$anh3);
			$anh3 = $_FILES["anh3"]["name"];
			move_uploaded_file($_FILES["anh3"]["tmp_name"], "../image/images_User/HinhSP/".$anh3);
		}

/*Đưa vào csdl*/
$suaDuLieu = "UPDATE sanpham 
SET TenSanPham='".$tensanpham."' , SoLuong = '".$soluong."' , Anh='".$anh1."' , Anh2='".$anh2."' , Anh3= '".$anh3."' , DonGia = '".$dongia."' , ThongTin = '".$thongtin."' , TrangThai='".$trangthai."'
, MaLoaiSP= '".$loaisp."'
WHERE MaSanPham='".$_GET["MaSanPham"]."' ";
	//print_r($suaDuLieu);exit();
	if(mysql_query($suaDuLieu)) {
		echo "<script>alert('Bạn đã Sửa thành công sản phẩm');</script>";
		echo "<script>location='SanPham.php';</script>";
	}else {
		echo "<script>alert('Bạn đã Sửa sản phẩm lỗi');</script>";
	}
	/*doan script do o dau vay?*/
}
?>
<?php
	include("../layout/footer_admin.php");

//$_FILES["Anh"]["type"]!= "image/jpeg";
//$_FILES["spAnh"]["type"] != "image/png";
//file_exists()
//unlink()
?>

