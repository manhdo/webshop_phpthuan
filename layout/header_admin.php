<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hệ thống Admin </title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/css_Admin/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/css_Admin/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- jQuery -->
    <script src="../script/js_Admin/jquery.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.../script/js_Admin/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php 
    //Khai báo  session! (khởi động session ở đây để admin có thể vào trang chủ sau khi đăng nhập thành công)
    session_start();

    /*Khởi tạo kết nối databse*/
    $ketnoi = mysql_connect("localhost", "root","");
    if(!$ketnoi) {
        echo "Lỗi kết nối đến cơ sở dữ liệu";
    }else{
        mysql_select_db("banhang_php", $ketnoi);
        mysql_query("set names utf8");
    }

    /*--------------Hàm phân trang php-------------*/
    function phan_trang($tenCot, $tenBang, $dieuKien, $soLuongSP, $trang,$dieuKienTrang){
        $spbatdau = $trang * $soLuongSP;

        $laySP = " SELECT ".$tenCot." FROM ".$tenBang." ".$dieuKien." LIMIT ".$spbatdau.",".$soLuongSP;
        
        $truyvanLaySP = mysql_query($laySP);

        $tongsoluongSP =  mysql_num_rows(mysql_query(" SELECT ".$tenCot." FROM ".$tenBang." ".$dieuKien));
        $tongsotrang =  $tongsoluongSP/$soLuongSP;

        /*Dùng để lưu tất cả các trang vào biến ds Trang*/
        $dsTrang = "";
        for($i=0; $i < $tongsotrang; $i++){
            $sotrang= $i+1;
            $dsTrang .=  "<a class='divphantrang_".$i."'' href=' ".$_SERVER["PHP_SELF"]."?trang=" .$i.$dieuKienTrang." '>". $sotrang  .  "</a>";
            /*ta có thể xóa luôn số trang vẫn sẽ hiểu*/
            //$dsTrang .=  "<a href='?trang=" .$i. " ' '>". $sotrang  .  "</a>";
        }
        echo "<script>
                $(document).ready(function(){
                    $('.divphantrang').html(\" ".$dsTrang."\");
                });
            </script>";
        return $truyvanLaySP;
    }

    // Hủy session khi nhấn đăng xuất 
    // if(isset($_GET["logout_admin"]))
    //     unset($_SESSION["admin"]); 

    /*Hàm định dạng tiền*/
    function DinhDangTien($dongia) { //Ví dụ: 2000000
        $sResult = $dongia;
        for($i=3; $i < strlen($sResult); $i+=4) {
            $sSau = substr($sResult, strlen($sResult) - $i);// cắt sau có nghĩa cắt đoạn sau theo cắt chuỗi.ở đây lần 1 cắt từ vị trí thứ 4 và cắt đc 3 số 000 ở cuối.
            $sDau = substr($sResult,0,  strlen($sResult) - $i);//Cắt đầu..Lần thứ nhất cắt từ vị trí số 0(tức là số 2) cho đến vị trí thứ 4
            $sResult = $sDau . "." . $sSau;//Sau khi thêm dấu . thì số ban đầu đã có 8 kí tự (2000.000)
        }
        return $sResult;
    }

/**Query lấy trạng thái đơn đặt hàng để ra chuông thông báo**/
$soDDH = mysql_num_rows(mysql_query("SELECT * FROM dondat WHERE TrangThai='0' "));
    
     // Hủy session khi nhấn đăng xuất 
//Nếu hủy session thành công thì trở về ngay trang index còn nếu hủy ko thành công thì ở lại trang cũ
    if(isset($_GET["logout_admin"])){
        unset($_SESSION["admin"]); 
        echo "<script>location='../page_admin/web/index.php';</script>";
    }
else{

?>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                           <a href="../page_admin/DonDatHang.php" class="dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bell"></i>
                                  <span style="color:red;">
                                           <?php
                                                 if($soDDH >= 100){
                                                    echo "99+";
                                                 }else {
                                                    echo  $soDDH;
                                                 }
                                           ?>
                                  </span>
                          </a>
                </li>
                <li class="dropdown">
<!--  Có 1 lỗi nhỏ trong vấn đề đăng xuất có thông báo trong phần này là: 
        Khi admin nhấn đăng xuất thì redirect đến <?php //echo $_SERVER["PHP_SELF"];?>?logout_admin=0"> tức là load lại trang này và usset $_SESSION["admin"]
        điều đó sẽ khiến $_SESSION["admin"]; không còn tồn tại nữa vì thế sẽ có thông báo undefine $_SESSION["admin"] lỗi dòng đó. -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span style="color:red"><?php echo $_SESSION["admin"]; ?></span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ThanhVien.php"><i class="fa fa-fw fa-user"></i> Thành viên</a>
                        </li>
                        <li>
                            <a href="NhanVien.php"><i class="fa fa-fw fa-user"></i> Nhân viên</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo $_SERVER["PHP_SELF"];?>?logout_admin=0"><i class="fa fa-fw fa-power-off"></i> Đăng Xuất</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-home"></i> Trang Chủ</a>
                    </li>
                    <li>
                        <a href="SanPham.php"><i class="fa fa-fw fa-bar-chart-o"></i> Quản lý sản phẩm</a>
                    </li>
                    <li>
                        <a href="DonDatHang.php"><i class="fa fa-fw fa-table"></i> Đơn đặt hàng</a>
                    </li>
                    <li>
                        <a href="BinhLuan.php"><i class="fa fa-fw fa-edit"></i> Bình luận sản phẩm</a>
                    </li>
                    <li>
                        <a href="DanhMucSanPham.php"><i class="fa fa-fw fa-desktop"></i> Danh mục sản phẩm</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
<?php
}
?>