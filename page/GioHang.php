<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
	include_once('../layout/header.php');

	/*check session giỏ hàng*/
	if(!isset($_SESSION["giohang"]))
		echo "<script>location='products.php';</script>";
?>
	<!--start-breadcrumbs-->
	<div class="breadcrumbs">
		<div class="container">
			<div class="breadcrumbs-main">
				<ol class="breadcrumb">
					<li><a href="index.php">Trang Chủ</a></li>
					<li class="active">Giỏ Hàng</li>
				</ol>
			</div>
		</div>
	</div>
	<!--end-breadcrumbs-->
	<!--start-ckeckout-->
<div id="giohang">
	<div class="ckeckout" style="padding-bottom:0px;">
		<div class="container">
			<div class="ckeckout-top">
			<div class=" cart-items heading">
			 <h3>Giỏ Hàng</h3>
			
				
			<div class="in-check" >
				<ul class="unit">
					<li><span></span></li>
					<li><span>Tên Sản Phẩm</span></li>		
					<li><span>Đơn Giá</span></li>
					<li><span>Số lượng</span></li>
					<li><span>Thành Tiền</span></li>
					<li> </li>
					<div class="clearfix"> </div>
				</ul>

<!-- Chú ý các dữ liệu đc lấy từ trang ThemGioHangAjax.php -->
<?php
	/*Khai báo tongtienGioHang dùng để tính tổng số tiền để hiển thị sang trang đặt hàng*/
	$tongtienGioHang = 0;
	foreach ($_SESSION["giohang"] as $cotGioHang) {
		# code...
	$tongtienGioHang += $cotGioHang["dongia"]*$cotGioHang["soluong"]; 	
?>
				<ul class="cart-header">
					<div class="close1" onclick="XoaGioHang(<?php echo $cotGioHang["masp"]; ?>)"></div>
						<li class="ring-in"><a href="single.php" ><img style="width:110px;" src="../image/images_User/HinhSP/<?php echo $cotGioHang["hinhsp"] ?>" class="img-responsive" alt=""></a>
						</li>
						<li><span><?php echo $cotGioHang["tensp"] ?></span></li>
						<li><span><?php echo DinhDangTien($cotGioHang["dongia"])?> VNĐ</span></li>
						<li><span>
							<select id="soluongdathang" onchange="SuaGioHang(<?php echo $cotGioHang["masp"] ?>, $(this).val());">
								<?php  for($i = 1; $i < 7; $i++) {
									if($cotGioHang["soluong"] == $i)
									{
								?>
								         <option value="<?php echo $i ?>" selected><?php echo $i ?></option>
								<?php }else { ?>
									<option value="<?php echo $i ?>"><?php echo $i ?></option>
								<?php } 
								} ?>
							</select>

						</span></li>
						<li><span><?php echo DinhDangTien($cotGioHang["dongia"]*$cotGioHang["soluong"]) ?> VNĐ</span></li>
						<li></li>
					<div class="clearfix"> </div>
				</ul>

<?php
	}
?>				

			</div>
			</div>  
		 </div>
		</div>
	</div>
	<!--end-ckeckout-->
	<div class="container" style="text-align:right;">
		<?php if(isset($_SESSION["tendangnhap"])){ ?>
				<a class="add-cart cart-check" id="buttom_DatHang" style="cursor: pointer;margin-right: 12px;">Đặt hàng</a>
			<?php } else{?>
				<span class="text-danger" style="font-weight:700;font-size: 18px;margin-right: 18px;">Bạn cần đăng nhập để đặt hàng</span>
			<?php 	
				}
			?>
	</div>
</div>

<!-- dùng để check có $_SESSION["tendangnhap"] hay không -->
<?php 
	if(isset($_SESSION["tendangnhap"])) {
		$layThanhVien = "SELECT * FROM thanhvien WHERE TenDangNhap ='".$_SESSION["tendangnhap"]."' ";
		$truyvanLayThanhVien = mysql_query($layThanhVien);
		$cotTV = mysql_fetch_array($truyvanLayThanhVien);
?>
		<!--start-ckeckout-->
	<div  id="DatHang" class="ckeckout" style="display:none;">
		<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>" >
			<div class="container">
				<div class="ckeckout-top">
				         <div class=" cart-items heading">
					         <h3 style="margin-bottom:15px;">Đặt Hàng</h3>	
						<!-- <div class="in-check" > --> <!-- do 2 class="in-check" đều dùng chung css nên ở bên đặt hàng t xóa đi!-->
						<div class="in-dathang">
							<ul class="unit">
								<li><span>Thông tin người đặt</span></li>
								<li><span></span></li>
								<li><span>Ngày đặt</span></li>		
								<li><span>Tổng sản phẩm</span></li>
								<li><span>Tổng tiền</span></li>
								<div class="clearfix"> </div>
							</ul>
							<ul class="cart-header">
								<li class="ring-in"></li>
								<li>
									<span style="text-align:left;">
										Tên người đặt hàng: <div style="padding:5px;border:1px solid #8c8383;"><span style="margin-top:0px; font-size:14px;"><?php echo $cotTV["HoTen"]; ?></span></div><br />
										Nơi giao: <textarea type="text" id="noigiao" name="noigiao" style="width:200px;padding:5px;"><?php echo $cotTV["DiaChi"]; ?></textarea>
									</span>
								</li>
								<li><span></span></li>
								<li><span><?php echo date("d/m/Y"); ?></span></li> <!-- lấy giá trị ngày hiện tại đặt hàng -->
								<li><span><?php echo count($_SESSION["giohang"]); ?></span></li>
								<li><span><?php echo DinhDangTien($tongtienGioHang); ?> VNĐ</span></li>
								<li></li>
								<div class="clearfix"> </div>
							</ul>			
						</div>
				         </div>  
			         </div>
			</div>
			<div class="container" style="text-align:right;">
				<input type="submit" class="add-cart cart-check" id="submit_DatHang" style="font-size: 1.15em;margin-right:12px;background-color:#8c2830;color:white;padding: 8px 10px;border: 1px solid #ebebeb;" value="Đặt hàng" />
			</div>
		</form>
	</div>
	<!--end-ckeckout-->

	
<?php 
	}
	if($_SERVER["REQUEST_METHOD"] == "POST") {
	         if(isset($_SESSION["giohang"])){
			$tendangnhap = $_SESSION["tendangnhap"];
			$trangthai = "0";
			$noigiao = $_POST["noigiao"];
			$ngaydat = date("Y-m-d");
/*Khi insert thì cần insert mặc định 1 mã nhân viên do manhanvien là khóa ngoại*/
			$themDonDat = "INSERT INTO dondat (TenDangNhap, MaNhanVien, TrangThai, NoiGiao,NgayDat) VALUES ('".$tendangnhap."' , '1' , '".$trangthai."' , '".$noigiao."' , '".$ngaydat ."' ) ";		
/*check nếu thêm thành công thì ms chuyển tới bước 2*/
			if(mysql_query($themDonDat)){
/*bước số 2 là query lấy ra mã đơn đặt là mã cuối cúng của bản dondat(vì khi ta insert thêm vào thì dữ liệu luôn ở phía dưới cùng).Mục đích lấy mã đơn đặt để insert tiếp số lượng và mã sản phẩm vào bảng ct_dondat*/
				$madondat = 0;
				$layDonDat = "SELECT * FROM dondat ORDER BY MaDonDat";
				$truyvan_layDonDat = mysql_query($layDonDat);
				while ($cotDD = mysql_fetch_array($truyvan_layDonDat)) {
					$madondat = $cotDD["MaDonDat"];
				}
			/*Duyệt session giỏ hàng để lấy ra cột masp và cột số lượng*/
				foreach ($_SESSION["giohang"] as $cotGioHang) {
					$masp = $cotGioHang["masp"]; /*chú lấy mã sản phẩm và soluong theo cotGioHang.*/
					$soluong = $cotGioHang["soluong"];

				/*Tiến hành thêm chi tiết đơn đặt hàng*/
					$themct_dondat = "INSERT INTO ct_dondat VALUES('".$madondat."' , '".$masp."' , '".$soluong."')";
					mysql_query($themct_dondat);	
				}
					echo "<script>alert('Bạn đã đặt hàng thành công');</script>";/*chú ý việc để echo này bên ngoài foreach vì để bên trong khi lặp 10 sản phẩm thêm vào thì sẽ alert ra 10 lần*/
					/*Đồng thời ta cũng phải xóa session giohang khi đã đặt hàng thành công*/
					unset($_SESSION["giohang"]);
					/*cần phải load lại trang 1 lần nữa để check session giỏ hàng..nếu ko có session thì tự động load sang trang products*/
					echo "<script>location='giohang.php';</script>"; 
		         }else{
		         	echo "<script>alert('Đã xảy ra lỗi khi thêm đơn hàng');</script>";
		         }
	        }else{
			echo "<script>alert('Đã xảy ra lỗi không tồn tại session giỏ hàng tức giohang rỗng');</script>";
		}
	}
?>
<!-- Nếu muốn để người dùng biết được giỏ hàng và đơn đặt hàng chi tiết thì ta sẽ không cần sử dụng đoạn script ẩn hiện này nữa -->
	<!--start-footer-->
<script src="../script/js_User/ajax_products.js"></script>
<script type="text/javascript">
	$(document).on('click', '#buttom_DatHang', function() {
		$('#giohang').slideUp();
		$('#DatHang').show();
	});
</script>
<?php
	include_once('../layout/footer.php');
?>